package pl.szczecin.sondecki.javaee.messages_provider.service.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.plugins.validation.hibernate.ValidateRequest;

import pl.szczecin.sondecki.javaee.messages_provider.model.Message;
import pl.szczecin.sondecki.javaee.messages_provider.model.Type;
import pl.szczecin.sondecki.javaee.messages_provider.service.repository.MessagesRepository;

/**
 * @author sondemar
 *
 *         RESTfil service responsible for adding new message, retrieving stored
 *         messages based on pagination and retrieving number of all messages.
 */
@Path("rest")
@RequestScoped
@ValidateRequest
public class MessagesResource {

	@EJB
	private MessagesRepository repository;

	/**
	 * Add message.
	 * 
	 * @param text
	 *            content
	 * @param type
	 *            type of message
	 */
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public void addMessage(@FormParam("message") String text,
			@FormParam("type") @NotNull Type type) {
		Message messagesDTO = new Message();
		messagesDTO.setText(text);
		messagesDTO.setType(type);
		repository.create(messagesDTO);
	}

	/**
	 * Return chunk of messages based on pagination where must be provide start
	 * page number and size of the returned page.
	 * 
	 * @param pageNumber
	 *            start page number (indexed from 0)
	 * @param pageSize
	 *            size of the returned page
	 * @return chunk of messages
	 */
	@Path("{pageNumber}/{pageSize}")
	@GET
	@Produces({ "application/xml", "application/json" })
	public List<Message> getMessages(
			@PathParam("pageNumber") @NotNull @Min(0) int pageNumber,
			@PathParam("pageSize") @NotNull int pageSize) {
		return repository.findAll(pageNumber, pageSize);
	}

	/**
	 * @return number of all messages.
	 */
	@Path("/count")
	@GET
	@Produces({ "application/xml", "application/json" })
	public Long getMessagesCount() {
		return repository.countAll();
	}
}
