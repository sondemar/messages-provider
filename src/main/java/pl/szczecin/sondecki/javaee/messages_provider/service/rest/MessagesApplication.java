package pl.szczecin.sondecki.javaee.messages_provider.service.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author sondemar
 *
 *         JAX-RS activator with main path "/messages"
 */
@ApplicationPath("messages")
public class MessagesApplication extends Application {

}
