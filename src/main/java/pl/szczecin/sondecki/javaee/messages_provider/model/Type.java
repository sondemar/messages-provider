package pl.szczecin.sondecki.javaee.messages_provider.model;

/**
 * @author sondemar
 *
 */
public enum Type {
	IN, OUT
}
