package pl.szczecin.sondecki.javaee.messages_provider.service.mbean;

import java.util.List;

import javax.management.MXBean;

import pl.szczecin.sondecki.javaee.messages_provider.model.Type;

/**
 * @author sondemar
 *
 *         This is MXBean interface which is contract for managing by using JMX.
 *         There is also possibility to replace this declaration (with
 *         annotation) with MessagesMBeanMXBean name (exactly there should be
 *         suffix MXBean).
 */
@MXBean
public interface MessagesMBean {

	/**
	 * @param type
	 *            is type of filter which is selected
	 */
	void filter(Type type);

	/**
	 * @return selected filters
	 */
	List<Type> showFilters();

	/**
	 * @return all accessible filters
	 */
	List<Type> showFilterStates();

	/**
	 * Clear selected filters
	 */
	void deleteFilters();
}
