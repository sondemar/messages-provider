package pl.szczecin.sondecki.javaee.messages_provider.service.mbean;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.plugins.validation.hibernate.ValidateRequest;

import pl.szczecin.sondecki.javaee.messages_provider.model.Type;

/**
 * @author sondemar
 *
 *         This is MXBean implementation responsible for managing {@link Type}
 *         which are filters for {@link Message}. This class is also RESTful
 *         service on singletone EJB. It allowes to manage by using JMX console
 *         or by using EJB/REST client.
 */
@Singleton
@Startup
@LocalBean
@Lock(LockType.READ)
@ValidateRequest
@Path("mxbean")
public class MessagesManagedBean implements MessagesMBean {

	@Resource
	private SessionContext serviceContext;

	@Inject
	private Logger log;

	/**
	 * list of selected filters.
	 */
	private ConcurrentSkipListSet<Type> filters = new ConcurrentSkipListSet<Type>();
	private MBeanServer mbeanServer;
	private ObjectName objectName = null;
	private String name;

	@PostConstruct
	protected void startup() {
		this.name = this.getClass().getSimpleName();
		log.info("Starting : " + this.name);
		try {
			objectName = new ObjectName("MessagesProducer:type="
					+ this.getClass().getName());
			mbeanServer = ManagementFactory.getPlatformMBeanServer();
			mbeanServer.registerMBean(
					serviceContext.getBusinessObject(this.getClass()),
					objectName);
		} catch (Exception e) {
			throw new IllegalStateException("Error during registration of "
					+ this.name + " into JMX:" + e);
		}
	}

	@PreDestroy
	protected void destroy() {
		log.info("Destroying : " + this.name);
		try {
			mbeanServer.unregisterMBean(this.objectName);
		} catch (Exception e) {
			throw new IllegalStateException("Error during unregistration of "
					+ this.name + " into JMX:" + e, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pl.szczecin.sondecki.javaee.messages_provider.service.mbean.MessagesMBean
	 * #filter(pl.szczecin.sondecki.javaee.messages_provider.model.Type)
	 */
	@POST
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Lock(LockType.WRITE)
	@Override
	public void filter(@FormParam("type") @DefaultValue("IN") Type type) {
		filters.add(type);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pl.szczecin.sondecki.javaee.messages_provider.service.mbean.MessagesMBean
	 * #showFilters()
	 */
	@Path("/showFilters")
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Override
	public List<Type> showFilters() {
		return new ArrayList<Type>(filters);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pl.szczecin.sondecki.javaee.messages_provider.service.mbean.MessagesMBean
	 * #showFilterStates()
	 */
	@Path("/showFilterStates")
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Override
	public List<Type> showFilterStates() {
		return Arrays.asList(Type.values());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pl.szczecin.sondecki.javaee.messages_provider.service.mbean.MessagesMBean
	 * #deleteFilters()
	 */
	@DELETE
	@Lock(LockType.WRITE)
	@Override
	public void deleteFilters() {
		filters.clear();
	}

}
