package pl.szczecin.sondecki.javaee.messages_provider.service.repository;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import pl.szczecin.sondecki.javaee.messages_provider.model.Message;
import pl.szczecin.sondecki.javaee.messages_provider.model.Type;
import pl.szczecin.sondecki.javaee.messages_provider.service.mbean.MessagesManagedBean;

/**
 * @author sondemar
 *
 *         This class is responsible for persisting a new message to the
 *         database and also retrieving existing messages from database based on
 *         filters stored cached in {@link MessagesManagedBean#} instance.
 *         Exactly there will be return only instances of {@link Message} which
 *         will not contain filters from
 *         {@link MessagesManagedBean#showFilters()}.
 */
@Stateless
public class MessagesRepository {

	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	@EJB
	MessagesManagedBean messagesBean;

	/**
	 * Persist new {@link Message} into DB.
	 * 
	 * @param message
	 */
	public void create(final Message message) {
		log.info("Persisting message of type: " + message.getType()
				+ " with content: " + message.getText());
		em.persist(message);
	}

	/**
	 * Return messages after filtering. There is used pagination based on start
	 * page number and number of record returned.
	 * 
	 * @param pageNumber
	 *            start page number
	 * @param pageSize
	 *            number of returned records
	 * @return list of messages after filtering.
	 */
	public List<Message> findAll(int pageNumber, int pageSize) {
		log.info("Finding messages: " + pageNumber + " with page size: "
				+ pageSize);
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

		CriteriaQuery<Message> criteriaQuery = criteriaBuilder
				.createQuery(Message.class);
		Root<Message> root = criteriaQuery.from(Message.class);
		Predicate predicate = criteriaBuilder.conjunction();
		predicate = criteriaBuilder.not(root.<Type> get("type").in(
				messagesBean.showFilters()));
		criteriaQuery.select(root).where(predicate);

		return em.createQuery(criteriaQuery).setFirstResult(pageNumber)
				.setMaxResults(pageSize).getResultList();
	}

	/**
	 * @return whole number of records from the database after filtering.
	 */
	public Long countAll() {
		log.info("Count number of entities which have message type: ");
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

		CriteriaQuery<Long> countQuery = criteriaBuilder
				.createQuery(Long.class);
		Root<Message> root = countQuery.from(Message.class);
		Predicate predicate = criteriaBuilder.conjunction();
		predicate = criteriaBuilder.not(root.<Type> get("type").in(
				messagesBean.showFilters()));

		countQuery.select(criteriaBuilder.count(root)).where(predicate);

		return em.createQuery(countQuery).getSingleResult();
	}
}
