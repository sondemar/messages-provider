package pl.szczecin.sondecki.javaee.messages_provider.service.mbean;

import java.lang.management.ManagementFactory;

import javax.inject.Inject;
import javax.management.MBeanInfo;
import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import pl.szczecin.sondecki.javaee.messages_provider.model.Type;
import pl.szczecin.sondecki.javaee.messages_provider.resources.Resources;

/**
 * @author sondemar
 *
 *         This is test for {@link MessagesManagedBean} unit.
 */
@RunWith(Arquillian.class)
public class MessagesManagedBeanTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap
				.create(WebArchive.class, "test.war")
				.addClasses(MessagesManagedBean.class, MessagesMBean.class,
						Type.class, Resources.class)
				.addAsResource("META-INF/test-persistence.xml",
						"META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.addAsWebInfResource("test-ds.xml");
	}

	@Inject
	MessagesManagedBean messagesManagedBean;

	/**
	 * This is simulation test of JMX console.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testMXBean() throws Exception {
		MBeanServer mbeanServer = ManagementFactory.getPlatformMBeanServer();
		ObjectName objectName = new ObjectName("MessagesProducer:type="
				+ MessagesManagedBean.class.getName());
		MBeanInfo mbeanInfo = mbeanServer.getMBeanInfo(objectName);
		Assert.assertNotNull(mbeanInfo);
		mbeanServer.invoke(objectName, "filter",
				new Object[] { Type.IN.name() },
				new String[] { "java.lang.String" });
		Assert.assertEquals(messagesManagedBean.showFilters().get(0), Type.IN);
		mbeanServer.invoke(objectName, "deleteFilters", new Object[] {},
				new String[] {});
		Assert.assertTrue(messagesManagedBean.showFilters().size() == 0);
		Assert.assertTrue(messagesManagedBean.showFilterStates().size() == Type
				.values().length);
	}
}
