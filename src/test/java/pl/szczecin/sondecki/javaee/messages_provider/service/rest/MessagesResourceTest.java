package pl.szczecin.sondecki.javaee.messages_provider.service.rest;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import pl.szczecin.sondecki.javaee.messages_provider.model.Message;
import pl.szczecin.sondecki.javaee.messages_provider.model.Type;
import pl.szczecin.sondecki.javaee.messages_provider.resources.Resources;
import pl.szczecin.sondecki.javaee.messages_provider.service.mbean.MessagesMBean;
import pl.szczecin.sondecki.javaee.messages_provider.service.mbean.MessagesManagedBean;
import pl.szczecin.sondecki.javaee.messages_provider.service.repository.MessagesRepository;

/**
 * @author sondemar
 *
 *         This is test for {@link MessagesResource} unit.
 */
@RunWith(Arquillian.class)
public class MessagesResourceTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap
				.create(WebArchive.class, "test.war")
				.addClasses(MessagesResource.class, MessagesRepository.class,
						Type.class, Message.class, Resources.class,
						MessagesApplication.class, MessagesManagedBean.class,
						MessagesMBean.class)
				.addAsResource("META-INF/test-persistence.xml",
						"META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.addAsWebInfResource("test-ds.xml");
	}

	private WebTarget target;

	@ArquillianResource
	private URL base;

	@Before
	public void setUp() throws MalformedURLException {
		Client client = ClientBuilder.newClient();
		target = client.target(URI.create(new URL(base, "messages/rest")
				.toExternalForm()));
		target.register(Message.class);
	}

	/**
	 * Test of POST and GET on MessagesResource REST service.
	 */
	@SuppressWarnings("serial")
	@Test
	@InSequence(1)
	public void testPostAndGet() {
		MultivaluedHashMap<String, String> map = new MultivaluedHashMap<>();
		map.add("message", "IN message");
		map.add("type", "IN");
		target.request().post(Entity.form(map));

		map.clear();
		map.add("message", "OUT message");
		map.add("type", "OUT");
		target.request().post(Entity.form(map));

		map.clear();
		map.add("message", "TEST");
		map.add("type", "IN");
		target.request().post(Entity.form(map));

		Long count = target.path("count").request(MediaType.APPLICATION_XML)
				.get(Long.class);

		List<Message> list = target.path("{pageNumber}/{pageSize}")
				.resolveTemplate("pageNumber", 0)
				.resolveTemplate("pageSize", 4)
				.request(MediaType.APPLICATION_XML)
				.get((new ArrayList<Message>() {
				}).getClass());
		assertEquals(count.intValue(), list.size());

	}

}
